# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :cloud,
  namespace: DI,
  ecto_repos: [DI.Repo]

# Configures the endpoint
config :cloud, DIWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ILd+6rtE7k1IpnYPy98mCvx8t5He+6xdKU7d7OFEkbtHiGqjORTIqE3GkTt5I6ak",
  render_errors: [view: DIWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DI.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
