defmodule DI.Production do
  @moduledoc """
  The Production context.
  """

  import Ecto.Query, warn: false
  alias DI.Repo

  alias DI.Production.Vendor

  @doc """
  Returns the list of vendors.

  ## Examples

      iex> list_vendors()
      [%Vendor{}, ...]

  """
  def list_vendors do
    Repo.all(Vendor)
  end

  @doc """
  Gets a single vendor.

  Raises `Ecto.NoResultsError` if the Vendor does not exist.

  ## Examples

      iex> get_vendor!(123)
      %Vendor{}

      iex> get_vendor!(456)
      ** (Ecto.NoResultsError)

  """
  def get_vendor!(id), do: Repo.get!(Vendor, id)

  @doc """
  Creates a vendor.

  ## Examples

      iex> create_vendor(%{field: value})
      {:ok, %Vendor{}}

      iex> create_vendor(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_vendor(attrs \\ %{}) do
    %Vendor{}
    |> Vendor.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a vendor.

  ## Examples

      iex> update_vendor(vendor, %{field: new_value})
      {:ok, %Vendor{}}

      iex> update_vendor(vendor, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_vendor(%Vendor{} = vendor, attrs) do
    vendor
    |> Vendor.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Vendor.

  ## Examples

      iex> delete_vendor(vendor)
      {:ok, %Vendor{}}

      iex> delete_vendor(vendor)
      {:error, %Ecto.Changeset{}}

  """
  def delete_vendor(%Vendor{} = vendor) do
    Repo.delete(vendor)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking vendor changes.

  ## Examples

      iex> change_vendor(vendor)
      %Ecto.Changeset{source: %Vendor{}}

  """
  def change_vendor(%Vendor{} = vendor) do
    Vendor.changeset(vendor, %{})
  end

  alias DI.Production.Cuisine

  @doc """
  Returns the list of cuisines.

  ## Examples

      iex> list_cuisines()
      [%Cuisine{}, ...]

  """
  def list_cuisines do
    Repo.all(Cuisine)
  end

  @doc """
  Gets a single cuisine.

  Raises `Ecto.NoResultsError` if the Cuisine does not exist.

  ## Examples

      iex> get_cuisine!(123)
      %Cuisine{}

      iex> get_cuisine!(456)
      ** (Ecto.NoResultsError)

  """
  def get_cuisine!(id), do: Repo.get!(Cuisine, id)

  @doc """
  Creates a cuisine.

  ## Examples

      iex> create_cuisine(%{field: value})
      {:ok, %Cuisine{}}

      iex> create_cuisine(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_cuisine(attrs \\ %{}) do
    %Cuisine{}
    |> Cuisine.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a cuisine.

  ## Examples

      iex> update_cuisine(cuisine, %{field: new_value})
      {:ok, %Cuisine{}}

      iex> update_cuisine(cuisine, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_cuisine(%Cuisine{} = cuisine, attrs) do
    cuisine
    |> Cuisine.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Cuisine.

  ## Examples

      iex> delete_cuisine(cuisine)
      {:ok, %Cuisine{}}

      iex> delete_cuisine(cuisine)
      {:error, %Ecto.Changeset{}}

  """
  def delete_cuisine(%Cuisine{} = cuisine) do
    Repo.delete(cuisine)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking cuisine changes.

  ## Examples

      iex> change_cuisine(cuisine)
      %Ecto.Changeset{source: %Cuisine{}}

  """
  def change_cuisine(%Cuisine{} = cuisine) do
    Cuisine.changeset(cuisine, %{})
  end

  alias DI.Production.Menu

  @doc """
  Returns the list of menus.

  ## Examples

      iex> list_menus()
      [%Menu{}, ...]

  """
  def list_menus do
    Repo.all(Menu)
  end

  @doc """
  Gets a single menu.

  Raises `Ecto.NoResultsError` if the Menu does not exist.

  ## Examples

      iex> get_menu!(123)
      %Menu{}

      iex> get_menu!(456)
      ** (Ecto.NoResultsError)

  """
  def get_menu!(id), do: Repo.get!(Menu, id)

  @doc """
  Creates a menu.

  ## Examples

      iex> create_menu(%{field: value})
      {:ok, %Menu{}}

      iex> create_menu(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_menu(attrs \\ %{}) do
    %Menu{}
    |> Menu.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a menu.

  ## Examples

      iex> update_menu(menu, %{field: new_value})
      {:ok, %Menu{}}

      iex> update_menu(menu, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_menu(%Menu{} = menu, attrs) do
    menu
    |> Menu.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Menu.

  ## Examples

      iex> delete_menu(menu)
      {:ok, %Menu{}}

      iex> delete_menu(menu)
      {:error, %Ecto.Changeset{}}

  """
  def delete_menu(%Menu{} = menu) do
    Repo.delete(menu)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking menu changes.

  ## Examples

      iex> change_menu(menu)
      %Ecto.Changeset{source: %Menu{}}

  """
  def change_menu(%Menu{} = menu) do
    Menu.changeset(menu, %{})
  end
end
