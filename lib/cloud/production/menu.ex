defmodule DI.Production.Menu do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Production.Menu


  schema "menus" do
    field :category, :string
    field :contains_egg, :boolean, default: false
    field :contains_meat, :boolean, default: false
    field :description, :string
    field :ingredients, {:array, :string}
    field :is_available, :boolean, default: false
    field :name, :string
    field :price, :integer
    field :vendor_id, :id
    field :cuisine_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Menu{} = menu, attrs) do
    menu
    |> cast(attrs, [:name, :price, :is_available, :category, :description, :ingredients, :contains_egg, :contains_meat])
    |> validate_required([:name, :price, :is_available, :category, :description, :ingredients, :contains_egg, :contains_meat])
  end
end
