defmodule DI.Production.Vendor do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Production.Vendor


  schema "vendors" do
    field :address, :string
    field :closes_at, :time
    field :is_open, :boolean, default: false
    field :is_verified, :boolean, default: false
    field :latitude, :float
    field :locality, :string
    field :longitude, :float
    field :name, :string
    field :opens_at, :time
    field :otp, :string
    field :phone, :string
    field :token, :string

    timestamps()
  end

  @doc false
  def changeset(%Vendor{} = vendor, attrs) do
    vendor
    |> cast(attrs, [:is_open, :name, :locality, :address, :opens_at, :closes_at, :token, :phone, :otp, :is_verified, :latitude, :longitude])
    |> validate_required([:is_open, :name, :locality, :address, :opens_at, :closes_at, :token, :phone, :otp, :is_verified, :latitude, :longitude])
  end
end
