defmodule DI.Production.Cuisine do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Production.Cuisine


  schema "cuisines" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(%Cuisine{} = cuisine, attrs) do
    cuisine
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
