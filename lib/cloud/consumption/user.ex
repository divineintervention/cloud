defmodule DI.Consumption.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Consumption.User


  schema "users" do
    field :is_verified, :boolean, default: false
    field :name, :string
    field :otp, :string
    field :phone, :string
    field :token, :string

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:token, :name, :phone, :otp, :is_verified])
    |> validate_required([:token, :name, :phone, :otp, :is_verified])
    |> unique_constraint(:phone)
  end
end
