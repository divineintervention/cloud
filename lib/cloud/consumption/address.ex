defmodule DI.Consumption.Address do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Consumption.Address


  schema "addresses" do
    field :city, :string
    field :label, :string
    field :landmark, :string
    field :latitude, :float
    field :longitude, :float
    field :pincode, :string
    field :street, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Address{} = address, attrs) do
    address
    |> cast(attrs, [:label, :street, :landmark, :city, :pincode, :latitude, :longitude])
    |> validate_required([:label, :street, :landmark, :city, :pincode, :latitude, :longitude])
  end
end
