defmodule DI.Logistics.Courier do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Logistics.Courier


  schema "couriers" do
    field :is_active, :boolean, default: false
    field :name, :string
    field :phone, :string

    timestamps()
  end

  @doc false
  def changeset(%Courier{} = courier, attrs) do
    courier
    |> cast(attrs, [:name, :phone, :is_active])
    |> validate_required([:name, :phone, :is_active])
    |> unique_constraint(:phone)
  end
end
