defmodule DI.Logistics.Bill do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Logistics.Bill


  schema "bills" do
    field :is_collected, :boolean, default: false
    field :vendor_id, :id
    field :delivery_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Bill{} = bill, attrs) do
    bill
    |> cast(attrs, [:is_collected])
    |> validate_required([:is_collected])
  end
end
