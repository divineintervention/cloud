defmodule DI.Logistics.BillItem do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Logistics.BillItem


  schema "billitems" do
    field :quantity, :integer
    field :bill_id, :id
    field :menu_id, :id

    timestamps()
  end

  @doc false
  def changeset(%BillItem{} = bill_item, attrs) do
    bill_item
    |> cast(attrs, [:quantity])
    |> validate_required([:quantity])
  end
end
