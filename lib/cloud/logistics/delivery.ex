defmodule DI.Logistics.Delivery do
  use Ecto.Schema
  import Ecto.Changeset
  alias DI.Logistics.Delivery


  schema "deliveries" do
    field :is_complete, :boolean, default: false
    field :courier_id, :id
    field :user_id, :id
    field :address_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Delivery{} = delivery, attrs) do
    delivery
    |> cast(attrs, [:is_complete])
    |> validate_required([:is_complete])
  end
end
