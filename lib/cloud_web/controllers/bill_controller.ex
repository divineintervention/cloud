defmodule DIWeb.BillController do
  use DIWeb, :controller

  alias DI.Logistics
  alias DI.Logistics.Bill

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    bills = Logistics.list_bills()
    render(conn, "index.json", bills: bills)
  end

  def create(conn, %{"bill" => bill_params}) do
    with {:ok, %Bill{} = bill} <- Logistics.create_bill(bill_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", bill_path(conn, :show, bill))
      |> render("show.json", bill: bill)
    end
  end

  def show(conn, %{"id" => id}) do
    bill = Logistics.get_bill!(id)
    render(conn, "show.json", bill: bill)
  end

  def update(conn, %{"id" => id, "bill" => bill_params}) do
    bill = Logistics.get_bill!(id)

    with {:ok, %Bill{} = bill} <- Logistics.update_bill(bill, bill_params) do
      render(conn, "show.json", bill: bill)
    end
  end

  def delete(conn, %{"id" => id}) do
    bill = Logistics.get_bill!(id)
    with {:ok, %Bill{}} <- Logistics.delete_bill(bill) do
      send_resp(conn, :no_content, "")
    end
  end
end
