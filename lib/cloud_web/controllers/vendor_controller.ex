defmodule DIWeb.VendorController do
  use DIWeb, :controller

  alias DI.Production
  alias DI.Production.Vendor

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    vendors = Production.list_vendors()
    render(conn, "index.json", vendors: vendors)
  end

  def create(conn, %{"vendor" => vendor_params}) do
    with {:ok, %Vendor{} = vendor} <- Production.create_vendor(vendor_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", vendor_path(conn, :show, vendor))
      |> render("show.json", vendor: vendor)
    end
  end

  def show(conn, %{"id" => id}) do
    vendor = Production.get_vendor!(id)
    render(conn, "show.json", vendor: vendor)
  end

  def update(conn, %{"id" => id, "vendor" => vendor_params}) do
    vendor = Production.get_vendor!(id)

    with {:ok, %Vendor{} = vendor} <- Production.update_vendor(vendor, vendor_params) do
      render(conn, "show.json", vendor: vendor)
    end
  end

  def delete(conn, %{"id" => id}) do
    vendor = Production.get_vendor!(id)
    with {:ok, %Vendor{}} <- Production.delete_vendor(vendor) do
      send_resp(conn, :no_content, "")
    end
  end
end
