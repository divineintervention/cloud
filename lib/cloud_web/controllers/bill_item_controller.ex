defmodule DIWeb.BillItemController do
  use DIWeb, :controller

  alias DI.Logistics
  alias DI.Logistics.BillItem

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    billitems = Logistics.list_billitems()
    render(conn, "index.json", billitems: billitems)
  end

  def create(conn, %{"bill_item" => bill_item_params}) do
    with {:ok, %BillItem{} = bill_item} <- Logistics.create_bill_item(bill_item_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", bill_item_path(conn, :show, bill_item))
      |> render("show.json", bill_item: bill_item)
    end
  end

  def show(conn, %{"id" => id}) do
    bill_item = Logistics.get_bill_item!(id)
    render(conn, "show.json", bill_item: bill_item)
  end

  def update(conn, %{"id" => id, "bill_item" => bill_item_params}) do
    bill_item = Logistics.get_bill_item!(id)

    with {:ok, %BillItem{} = bill_item} <- Logistics.update_bill_item(bill_item, bill_item_params) do
      render(conn, "show.json", bill_item: bill_item)
    end
  end

  def delete(conn, %{"id" => id}) do
    bill_item = Logistics.get_bill_item!(id)
    with {:ok, %BillItem{}} <- Logistics.delete_bill_item(bill_item) do
      send_resp(conn, :no_content, "")
    end
  end
end
