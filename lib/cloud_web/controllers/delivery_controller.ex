defmodule DIWeb.DeliveryController do
  use DIWeb, :controller

  alias DI.Logistics
  alias DI.Logistics.Delivery

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    deliveries = Logistics.list_deliveries()
    DIWeb.Endpoint.broadcast!("dispatch:deliveries", "new_msg", %{
          "message" => "This is awesome!",
          "hello" => "Whatever man!",
          "heha" => "hahe"})
    render(conn, "index.json", deliveries: deliveries)
  end

  def create(conn, %{"delivery" => delivery_params}) do
    with {:ok, %Delivery{} = delivery} <- Logistics.create_delivery(delivery_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", delivery_path(conn, :show, delivery))
      |> render("show.json", delivery: delivery)
    end
  end

  def show(conn, %{"id" => id}) do
    delivery = Logistics.get_delivery!(id)
    render(conn, "show.json", delivery: delivery)
  end

  def update(conn, %{"id" => id, "delivery" => delivery_params}) do
    delivery = Logistics.get_delivery!(id)

    with {:ok, %Delivery{} = delivery} <- Logistics.update_delivery(delivery, delivery_params) do
      render(conn, "show.json", delivery: delivery)
    end
  end

  def delete(conn, %{"id" => id}) do
    delivery = Logistics.get_delivery!(id)
    with {:ok, %Delivery{}} <- Logistics.delete_delivery(delivery) do
      send_resp(conn, :no_content, "")
    end
  end
end
