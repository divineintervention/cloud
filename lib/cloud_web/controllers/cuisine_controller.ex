defmodule DIWeb.CuisineController do
  use DIWeb, :controller

  alias DI.Production
  alias DI.Production.Cuisine

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    cuisines = Production.list_cuisines()
    render(conn, "index.json", cuisines: cuisines)
  end

  def create(conn, %{"cuisine" => cuisine_params}) do
    with {:ok, %Cuisine{} = cuisine} <- Production.create_cuisine(cuisine_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", cuisine_path(conn, :show, cuisine))
      |> render("show.json", cuisine: cuisine)
    end
  end

  def show(conn, %{"id" => id}) do
    cuisine = Production.get_cuisine!(id)
    render(conn, "show.json", cuisine: cuisine)
  end

  def update(conn, %{"id" => id, "cuisine" => cuisine_params}) do
    cuisine = Production.get_cuisine!(id)

    with {:ok, %Cuisine{} = cuisine} <- Production.update_cuisine(cuisine, cuisine_params) do
      render(conn, "show.json", cuisine: cuisine)
    end
  end

  def delete(conn, %{"id" => id}) do
    cuisine = Production.get_cuisine!(id)
    with {:ok, %Cuisine{}} <- Production.delete_cuisine(cuisine) do
      send_resp(conn, :no_content, "")
    end
  end
end
