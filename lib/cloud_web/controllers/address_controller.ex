defmodule DIWeb.AddressController do
  use DIWeb, :controller

  alias DI.Consumption
  alias DI.Consumption.Address

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    addresses = Consumption.list_addresses()
    render(conn, "index.json", addresses: addresses)
  end

  def create(conn, %{"address" => address_params}) do
    with {:ok, %Address{} = address} <- Consumption.create_address(address_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", address_path(conn, :show, address))
      |> render("show.json", address: address)
    end
  end

  def show(conn, %{"id" => id}) do
    address = Consumption.get_address!(id)
    render(conn, "show.json", address: address)
  end

  def update(conn, %{"id" => id, "address" => address_params}) do
    address = Consumption.get_address!(id)

    with {:ok, %Address{} = address} <- Consumption.update_address(address, address_params) do
      render(conn, "show.json", address: address)
    end
  end

  def delete(conn, %{"id" => id}) do
    address = Consumption.get_address!(id)
    with {:ok, %Address{}} <- Consumption.delete_address(address) do
      send_resp(conn, :no_content, "")
    end
  end
end
