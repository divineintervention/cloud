defmodule DIWeb.PageController do
  use DIWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
