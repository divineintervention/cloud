defmodule DIWeb.CourierController do
  use DIWeb, :controller

  alias DI.Logistics
  alias DI.Logistics.Courier

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    couriers = Logistics.list_couriers()
    render(conn, "index.json", couriers: couriers)
  end

  def create(conn, %{"courier" => courier_params}) do
    with {:ok, %Courier{} = courier} <- Logistics.create_courier(courier_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", courier_path(conn, :show, courier))
      |> render("show.json", courier: courier)
    end
  end

  def show(conn, %{"id" => id}) do
    courier = Logistics.get_courier!(id)
    render(conn, "show.json", courier: courier)
  end

  def update(conn, %{"id" => id, "courier" => courier_params}) do
    courier = Logistics.get_courier!(id)

    with {:ok, %Courier{} = courier} <- Logistics.update_courier(courier, courier_params) do
      render(conn, "show.json", courier: courier)
    end
  end

  def delete(conn, %{"id" => id}) do
    courier = Logistics.get_courier!(id)
    with {:ok, %Courier{}} <- Logistics.delete_courier(courier) do
      send_resp(conn, :no_content, "")
    end
  end
end
