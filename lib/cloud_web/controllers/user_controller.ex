defmodule DIWeb.UserController do
  use DIWeb, :controller

  alias DI.Consumption
  alias DI.Consumption.User

  action_fallback DIWeb.FallbackController

  def index(conn, _params) do
    users = Consumption.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Consumption.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Consumption.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Consumption.get_user!(id)

    with {:ok, %User{} = user} <- Consumption.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Consumption.get_user!(id)
    with {:ok, %User{}} <- Consumption.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
