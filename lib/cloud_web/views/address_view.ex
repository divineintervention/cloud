defmodule DIWeb.AddressView do
  use DIWeb, :view
  alias DIWeb.AddressView

  def render("index.json", %{addresses: addresses}) do
    %{data: render_many(addresses, AddressView, "address.json")}
  end

  def render("show.json", %{address: address}) do
    %{data: render_one(address, AddressView, "address.json")}
  end

  def render("address.json", %{address: address}) do
    %{id: address.id,
      label: address.label,
      street: address.street,
      landmark: address.landmark,
      city: address.city,
      pincode: address.pincode,
      latitude: address.latitude,
      longitude: address.longitude}
  end
end
