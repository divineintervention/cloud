defmodule DIWeb.BillItemView do
  use DIWeb, :view
  alias DIWeb.BillItemView

  def render("index.json", %{billitems: billitems}) do
    %{data: render_many(billitems, BillItemView, "bill_item.json")}
  end

  def render("show.json", %{bill_item: bill_item}) do
    %{data: render_one(bill_item, BillItemView, "bill_item.json")}
  end

  def render("bill_item.json", %{bill_item: bill_item}) do
    %{id: bill_item.id,
      quantity: bill_item.quantity}
  end
end
