defmodule DIWeb.CourierView do
  use DIWeb, :view
  alias DIWeb.CourierView

  def render("index.json", %{couriers: couriers}) do
    %{data: render_many(couriers, CourierView, "courier.json")}
  end

  def render("show.json", %{courier: courier}) do
    %{data: render_one(courier, CourierView, "courier.json")}
  end

  def render("courier.json", %{courier: courier}) do
    %{id: courier.id,
      name: courier.name,
      phone: courier.phone,
      is_active: courier.is_active}
  end
end
