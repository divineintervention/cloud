defmodule DIWeb.BillView do
  use DIWeb, :view
  alias DIWeb.BillView

  def render("index.json", %{bills: bills}) do
    %{data: render_many(bills, BillView, "bill.json")}
  end

  def render("show.json", %{bill: bill}) do
    %{data: render_one(bill, BillView, "bill.json")}
  end

  def render("bill.json", %{bill: bill}) do
    %{id: bill.id,
      is_collected: bill.is_collected}
  end
end
