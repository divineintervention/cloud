defmodule DIWeb.MenuView do
  use DIWeb, :view
  alias DIWeb.MenuView

  def render("index.json", %{menus: menus}) do
    %{data: render_many(menus, MenuView, "menu.json")}
  end

  def render("show.json", %{menu: menu}) do
    %{data: render_one(menu, MenuView, "menu.json")}
  end

  def render("menu.json", %{menu: menu}) do
    %{id: menu.id,
      name: menu.name,
      price: menu.price,
      is_available: menu.is_available,
      category: menu.category,
      description: menu.description,
      ingredients: menu.ingredients,
      contains_egg: menu.contains_egg,
      contains_meat: menu.contains_meat}
  end
end
