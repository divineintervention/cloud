defmodule DIWeb.VendorView do
  use DIWeb, :view
  alias DIWeb.VendorView

  def render("index.json", %{vendors: vendors}) do
    %{data: render_many(vendors, VendorView, "vendor.json")}
  end

  def render("show.json", %{vendor: vendor}) do
    %{data: render_one(vendor, VendorView, "vendor.json")}
  end

  def render("vendor.json", %{vendor: vendor}) do
    %{id: vendor.id,
      is_open: vendor.is_open,
      name: vendor.name,
      locality: vendor.locality,
      address: vendor.address,
      opens_at: vendor.opens_at,
      closes_at: vendor.closes_at,
      token: vendor.token,
      phone: vendor.phone,
      otp: vendor.otp,
      is_verified: vendor.is_verified,
      latitude: vendor.latitude,
      longitude: vendor.longitude}
  end
end
