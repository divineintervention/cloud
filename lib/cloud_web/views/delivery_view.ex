defmodule DIWeb.DeliveryView do
  use DIWeb, :view
  alias DIWeb.DeliveryView

  def render("index.json", %{deliveries: deliveries}) do
    %{data: render_many(deliveries, DeliveryView, "delivery.json")}
  end

  def render("show.json", %{delivery: delivery}) do
    %{data: render_one(delivery, DeliveryView, "delivery.json")}
  end

  def render("delivery.json", %{delivery: delivery}) do
    %{id: delivery.id,
      is_complete: delivery.is_complete}
  end
end
