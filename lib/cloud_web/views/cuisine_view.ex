defmodule DIWeb.CuisineView do
  use DIWeb, :view
  alias DIWeb.CuisineView

  def render("index.json", %{cuisines: cuisines}) do
    %{data: render_many(cuisines, CuisineView, "cuisine.json")}
  end

  def render("show.json", %{cuisine: cuisine}) do
    %{data: render_one(cuisine, CuisineView, "cuisine.json")}
  end

  def render("cuisine.json", %{cuisine: cuisine}) do
    %{id: cuisine.id,
      name: cuisine.name}
  end
end
