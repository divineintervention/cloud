defmodule DIWeb.DispatchChannel do
  use Phoenix.Channel

  def join("dispatch:deliveries", _message, socket) do
    {:ok, socket}
  end
  def join("dispatch:*", _message, _socket) do
    {:error, %{reason: "Unknown channel topic"}}
  end
end
