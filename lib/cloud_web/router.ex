defmodule DIWeb.Router do
  use DIWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DIWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", DIWeb do
    pipe_through :api

    resources "/couriers", CourierController, except: [:new, :edit]
    resources "/users", UserController, except: [:new, :edit]
    resources "/addresses", AddressController, except: [:new, :edit]
    resources "/vendors", VendorController, except: [:new, :edit]
    resources "/cuisines", CuisineController, except: [:new, :edit]
    resources "/menus", MenuController, except: [:new, :edit]
    resources "/deliveries", DeliveryController, except: [:new, :edit]
    resources "/bills", BillController, except: [:new, :edit]
    resources "/billitems", BillItemController, except: [:new, :edit]
  end
end
