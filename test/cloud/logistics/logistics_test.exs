defmodule DI.LogisticsTest do
  use DI.DataCase

  alias DI.Logistics

  describe "couriers" do
    alias DI.Logistics.Courier

    @valid_attrs %{is_active: true, name: "some name", phone: "some phone"}
    @update_attrs %{is_active: false, name: "some updated name", phone: "some updated phone"}
    @invalid_attrs %{is_active: nil, name: nil, phone: nil}

    def courier_fixture(attrs \\ %{}) do
      {:ok, courier} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Logistics.create_courier()

      courier
    end

    test "list_couriers/0 returns all couriers" do
      courier = courier_fixture()
      assert Logistics.list_couriers() == [courier]
    end

    test "get_courier!/1 returns the courier with given id" do
      courier = courier_fixture()
      assert Logistics.get_courier!(courier.id) == courier
    end

    test "create_courier/1 with valid data creates a courier" do
      assert {:ok, %Courier{} = courier} = Logistics.create_courier(@valid_attrs)
      assert courier.is_active == true
      assert courier.name == "some name"
      assert courier.phone == "some phone"
    end

    test "create_courier/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Logistics.create_courier(@invalid_attrs)
    end

    test "update_courier/2 with valid data updates the courier" do
      courier = courier_fixture()
      assert {:ok, courier} = Logistics.update_courier(courier, @update_attrs)
      assert %Courier{} = courier
      assert courier.is_active == false
      assert courier.name == "some updated name"
      assert courier.phone == "some updated phone"
    end

    test "update_courier/2 with invalid data returns error changeset" do
      courier = courier_fixture()
      assert {:error, %Ecto.Changeset{}} = Logistics.update_courier(courier, @invalid_attrs)
      assert courier == Logistics.get_courier!(courier.id)
    end

    test "delete_courier/1 deletes the courier" do
      courier = courier_fixture()
      assert {:ok, %Courier{}} = Logistics.delete_courier(courier)
      assert_raise Ecto.NoResultsError, fn -> Logistics.get_courier!(courier.id) end
    end

    test "change_courier/1 returns a courier changeset" do
      courier = courier_fixture()
      assert %Ecto.Changeset{} = Logistics.change_courier(courier)
    end
  end

  describe "deliveries" do
    alias DI.Logistics.Delivery

    @valid_attrs %{is_complete: true}
    @update_attrs %{is_complete: false}
    @invalid_attrs %{is_complete: nil}

    def delivery_fixture(attrs \\ %{}) do
      {:ok, delivery} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Logistics.create_delivery()

      delivery
    end

    test "list_deliveries/0 returns all deliveries" do
      delivery = delivery_fixture()
      assert Logistics.list_deliveries() == [delivery]
    end

    test "get_delivery!/1 returns the delivery with given id" do
      delivery = delivery_fixture()
      assert Logistics.get_delivery!(delivery.id) == delivery
    end

    test "create_delivery/1 with valid data creates a delivery" do
      assert {:ok, %Delivery{} = delivery} = Logistics.create_delivery(@valid_attrs)
      assert delivery.is_complete == true
    end

    test "create_delivery/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Logistics.create_delivery(@invalid_attrs)
    end

    test "update_delivery/2 with valid data updates the delivery" do
      delivery = delivery_fixture()
      assert {:ok, delivery} = Logistics.update_delivery(delivery, @update_attrs)
      assert %Delivery{} = delivery
      assert delivery.is_complete == false
    end

    test "update_delivery/2 with invalid data returns error changeset" do
      delivery = delivery_fixture()
      assert {:error, %Ecto.Changeset{}} = Logistics.update_delivery(delivery, @invalid_attrs)
      assert delivery == Logistics.get_delivery!(delivery.id)
    end

    test "delete_delivery/1 deletes the delivery" do
      delivery = delivery_fixture()
      assert {:ok, %Delivery{}} = Logistics.delete_delivery(delivery)
      assert_raise Ecto.NoResultsError, fn -> Logistics.get_delivery!(delivery.id) end
    end

    test "change_delivery/1 returns a delivery changeset" do
      delivery = delivery_fixture()
      assert %Ecto.Changeset{} = Logistics.change_delivery(delivery)
    end
  end

  describe "bills" do
    alias DI.Logistics.Bill

    @valid_attrs %{is_collected: true}
    @update_attrs %{is_collected: false}
    @invalid_attrs %{is_collected: nil}

    def bill_fixture(attrs \\ %{}) do
      {:ok, bill} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Logistics.create_bill()

      bill
    end

    test "list_bills/0 returns all bills" do
      bill = bill_fixture()
      assert Logistics.list_bills() == [bill]
    end

    test "get_bill!/1 returns the bill with given id" do
      bill = bill_fixture()
      assert Logistics.get_bill!(bill.id) == bill
    end

    test "create_bill/1 with valid data creates a bill" do
      assert {:ok, %Bill{} = bill} = Logistics.create_bill(@valid_attrs)
      assert bill.is_collected == true
    end

    test "create_bill/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Logistics.create_bill(@invalid_attrs)
    end

    test "update_bill/2 with valid data updates the bill" do
      bill = bill_fixture()
      assert {:ok, bill} = Logistics.update_bill(bill, @update_attrs)
      assert %Bill{} = bill
      assert bill.is_collected == false
    end

    test "update_bill/2 with invalid data returns error changeset" do
      bill = bill_fixture()
      assert {:error, %Ecto.Changeset{}} = Logistics.update_bill(bill, @invalid_attrs)
      assert bill == Logistics.get_bill!(bill.id)
    end

    test "delete_bill/1 deletes the bill" do
      bill = bill_fixture()
      assert {:ok, %Bill{}} = Logistics.delete_bill(bill)
      assert_raise Ecto.NoResultsError, fn -> Logistics.get_bill!(bill.id) end
    end

    test "change_bill/1 returns a bill changeset" do
      bill = bill_fixture()
      assert %Ecto.Changeset{} = Logistics.change_bill(bill)
    end
  end

  describe "billitems" do
    alias DI.Logistics.BillItem

    @valid_attrs %{quantity: 42}
    @update_attrs %{quantity: 43}
    @invalid_attrs %{quantity: nil}

    def bill_item_fixture(attrs \\ %{}) do
      {:ok, bill_item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Logistics.create_bill_item()

      bill_item
    end

    test "list_billitems/0 returns all billitems" do
      bill_item = bill_item_fixture()
      assert Logistics.list_billitems() == [bill_item]
    end

    test "get_bill_item!/1 returns the bill_item with given id" do
      bill_item = bill_item_fixture()
      assert Logistics.get_bill_item!(bill_item.id) == bill_item
    end

    test "create_bill_item/1 with valid data creates a bill_item" do
      assert {:ok, %BillItem{} = bill_item} = Logistics.create_bill_item(@valid_attrs)
      assert bill_item.quantity == 42
    end

    test "create_bill_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Logistics.create_bill_item(@invalid_attrs)
    end

    test "update_bill_item/2 with valid data updates the bill_item" do
      bill_item = bill_item_fixture()
      assert {:ok, bill_item} = Logistics.update_bill_item(bill_item, @update_attrs)
      assert %BillItem{} = bill_item
      assert bill_item.quantity == 43
    end

    test "update_bill_item/2 with invalid data returns error changeset" do
      bill_item = bill_item_fixture()
      assert {:error, %Ecto.Changeset{}} = Logistics.update_bill_item(bill_item, @invalid_attrs)
      assert bill_item == Logistics.get_bill_item!(bill_item.id)
    end

    test "delete_bill_item/1 deletes the bill_item" do
      bill_item = bill_item_fixture()
      assert {:ok, %BillItem{}} = Logistics.delete_bill_item(bill_item)
      assert_raise Ecto.NoResultsError, fn -> Logistics.get_bill_item!(bill_item.id) end
    end

    test "change_bill_item/1 returns a bill_item changeset" do
      bill_item = bill_item_fixture()
      assert %Ecto.Changeset{} = Logistics.change_bill_item(bill_item)
    end
  end
end
