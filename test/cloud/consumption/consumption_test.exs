defmodule DI.ConsumptionTest do
  use DI.DataCase

  alias DI.Consumption

  describe "users" do
    alias DI.Consumption.User

    @valid_attrs %{is_verified: true, name: "some name", otp: "some otp", phone: "some phone", token: "some token"}
    @update_attrs %{is_verified: false, name: "some updated name", otp: "some updated otp", phone: "some updated phone", token: "some updated token"}
    @invalid_attrs %{is_verified: nil, name: nil, otp: nil, phone: nil, token: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Consumption.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Consumption.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Consumption.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Consumption.create_user(@valid_attrs)
      assert user.is_verified == true
      assert user.name == "some name"
      assert user.otp == "some otp"
      assert user.phone == "some phone"
      assert user.token == "some token"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Consumption.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Consumption.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.is_verified == false
      assert user.name == "some updated name"
      assert user.otp == "some updated otp"
      assert user.phone == "some updated phone"
      assert user.token == "some updated token"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Consumption.update_user(user, @invalid_attrs)
      assert user == Consumption.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Consumption.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Consumption.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Consumption.change_user(user)
    end
  end

  describe "addresses" do
    alias DI.Consumption.Address

    @valid_attrs %{city: "some city", label: "some label", landmark: "some landmark", latitude: 120.5, longitude: 120.5, pincode: "some pincode", street: "some street"}
    @update_attrs %{city: "some updated city", label: "some updated label", landmark: "some updated landmark", latitude: 456.7, longitude: 456.7, pincode: "some updated pincode", street: "some updated street"}
    @invalid_attrs %{city: nil, label: nil, landmark: nil, latitude: nil, longitude: nil, pincode: nil, street: nil}

    def address_fixture(attrs \\ %{}) do
      {:ok, address} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Consumption.create_address()

      address
    end

    test "list_addresses/0 returns all addresses" do
      address = address_fixture()
      assert Consumption.list_addresses() == [address]
    end

    test "get_address!/1 returns the address with given id" do
      address = address_fixture()
      assert Consumption.get_address!(address.id) == address
    end

    test "create_address/1 with valid data creates a address" do
      assert {:ok, %Address{} = address} = Consumption.create_address(@valid_attrs)
      assert address.city == "some city"
      assert address.label == "some label"
      assert address.landmark == "some landmark"
      assert address.latitude == 120.5
      assert address.longitude == 120.5
      assert address.pincode == "some pincode"
      assert address.street == "some street"
    end

    test "create_address/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Consumption.create_address(@invalid_attrs)
    end

    test "update_address/2 with valid data updates the address" do
      address = address_fixture()
      assert {:ok, address} = Consumption.update_address(address, @update_attrs)
      assert %Address{} = address
      assert address.city == "some updated city"
      assert address.label == "some updated label"
      assert address.landmark == "some updated landmark"
      assert address.latitude == 456.7
      assert address.longitude == 456.7
      assert address.pincode == "some updated pincode"
      assert address.street == "some updated street"
    end

    test "update_address/2 with invalid data returns error changeset" do
      address = address_fixture()
      assert {:error, %Ecto.Changeset{}} = Consumption.update_address(address, @invalid_attrs)
      assert address == Consumption.get_address!(address.id)
    end

    test "delete_address/1 deletes the address" do
      address = address_fixture()
      assert {:ok, %Address{}} = Consumption.delete_address(address)
      assert_raise Ecto.NoResultsError, fn -> Consumption.get_address!(address.id) end
    end

    test "change_address/1 returns a address changeset" do
      address = address_fixture()
      assert %Ecto.Changeset{} = Consumption.change_address(address)
    end
  end
end
