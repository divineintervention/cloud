defmodule DI.ProductionTest do
  use DI.DataCase

  alias DI.Production

  describe "vendors" do
    alias DI.Production.Vendor

    @valid_attrs %{address: "some address", closes_at: ~T[14:00:00.000000], is_open: true, is_verified: true, latitude: 120.5, locality: "some locality", longitude: 120.5, name: "some name", opens_at: ~T[14:00:00.000000], otp: "some otp", phone: "some phone", token: "some token"}
    @update_attrs %{address: "some updated address", closes_at: ~T[15:01:01.000000], is_open: false, is_verified: false, latitude: 456.7, locality: "some updated locality", longitude: 456.7, name: "some updated name", opens_at: ~T[15:01:01.000000], otp: "some updated otp", phone: "some updated phone", token: "some updated token"}
    @invalid_attrs %{address: nil, closes_at: nil, is_open: nil, is_verified: nil, latitude: nil, locality: nil, longitude: nil, name: nil, opens_at: nil, otp: nil, phone: nil, token: nil}

    def vendor_fixture(attrs \\ %{}) do
      {:ok, vendor} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Production.create_vendor()

      vendor
    end

    test "list_vendors/0 returns all vendors" do
      vendor = vendor_fixture()
      assert Production.list_vendors() == [vendor]
    end

    test "get_vendor!/1 returns the vendor with given id" do
      vendor = vendor_fixture()
      assert Production.get_vendor!(vendor.id) == vendor
    end

    test "create_vendor/1 with valid data creates a vendor" do
      assert {:ok, %Vendor{} = vendor} = Production.create_vendor(@valid_attrs)
      assert vendor.address == "some address"
      assert vendor.closes_at == ~T[14:00:00.000000]
      assert vendor.is_open == true
      assert vendor.is_verified == true
      assert vendor.latitude == 120.5
      assert vendor.locality == "some locality"
      assert vendor.longitude == 120.5
      assert vendor.name == "some name"
      assert vendor.opens_at == ~T[14:00:00.000000]
      assert vendor.otp == "some otp"
      assert vendor.phone == "some phone"
      assert vendor.token == "some token"
    end

    test "create_vendor/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Production.create_vendor(@invalid_attrs)
    end

    test "update_vendor/2 with valid data updates the vendor" do
      vendor = vendor_fixture()
      assert {:ok, vendor} = Production.update_vendor(vendor, @update_attrs)
      assert %Vendor{} = vendor
      assert vendor.address == "some updated address"
      assert vendor.closes_at == ~T[15:01:01.000000]
      assert vendor.is_open == false
      assert vendor.is_verified == false
      assert vendor.latitude == 456.7
      assert vendor.locality == "some updated locality"
      assert vendor.longitude == 456.7
      assert vendor.name == "some updated name"
      assert vendor.opens_at == ~T[15:01:01.000000]
      assert vendor.otp == "some updated otp"
      assert vendor.phone == "some updated phone"
      assert vendor.token == "some updated token"
    end

    test "update_vendor/2 with invalid data returns error changeset" do
      vendor = vendor_fixture()
      assert {:error, %Ecto.Changeset{}} = Production.update_vendor(vendor, @invalid_attrs)
      assert vendor == Production.get_vendor!(vendor.id)
    end

    test "delete_vendor/1 deletes the vendor" do
      vendor = vendor_fixture()
      assert {:ok, %Vendor{}} = Production.delete_vendor(vendor)
      assert_raise Ecto.NoResultsError, fn -> Production.get_vendor!(vendor.id) end
    end

    test "change_vendor/1 returns a vendor changeset" do
      vendor = vendor_fixture()
      assert %Ecto.Changeset{} = Production.change_vendor(vendor)
    end
  end

  describe "cuisines" do
    alias DI.Production.Cuisine

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def cuisine_fixture(attrs \\ %{}) do
      {:ok, cuisine} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Production.create_cuisine()

      cuisine
    end

    test "list_cuisines/0 returns all cuisines" do
      cuisine = cuisine_fixture()
      assert Production.list_cuisines() == [cuisine]
    end

    test "get_cuisine!/1 returns the cuisine with given id" do
      cuisine = cuisine_fixture()
      assert Production.get_cuisine!(cuisine.id) == cuisine
    end

    test "create_cuisine/1 with valid data creates a cuisine" do
      assert {:ok, %Cuisine{} = cuisine} = Production.create_cuisine(@valid_attrs)
      assert cuisine.name == "some name"
    end

    test "create_cuisine/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Production.create_cuisine(@invalid_attrs)
    end

    test "update_cuisine/2 with valid data updates the cuisine" do
      cuisine = cuisine_fixture()
      assert {:ok, cuisine} = Production.update_cuisine(cuisine, @update_attrs)
      assert %Cuisine{} = cuisine
      assert cuisine.name == "some updated name"
    end

    test "update_cuisine/2 with invalid data returns error changeset" do
      cuisine = cuisine_fixture()
      assert {:error, %Ecto.Changeset{}} = Production.update_cuisine(cuisine, @invalid_attrs)
      assert cuisine == Production.get_cuisine!(cuisine.id)
    end

    test "delete_cuisine/1 deletes the cuisine" do
      cuisine = cuisine_fixture()
      assert {:ok, %Cuisine{}} = Production.delete_cuisine(cuisine)
      assert_raise Ecto.NoResultsError, fn -> Production.get_cuisine!(cuisine.id) end
    end

    test "change_cuisine/1 returns a cuisine changeset" do
      cuisine = cuisine_fixture()
      assert %Ecto.Changeset{} = Production.change_cuisine(cuisine)
    end
  end

  describe "menus" do
    alias DI.Production.Menu

    @valid_attrs %{category: "some category", contains_egg: true, contains_meat: true, description: "some description", ingredients: [], is_available: true, name: "some name", price: 42}
    @update_attrs %{category: "some updated category", contains_egg: false, contains_meat: false, description: "some updated description", ingredients: [], is_available: false, name: "some updated name", price: 43}
    @invalid_attrs %{category: nil, contains_egg: nil, contains_meat: nil, description: nil, ingredients: nil, is_available: nil, name: nil, price: nil}

    def menu_fixture(attrs \\ %{}) do
      {:ok, menu} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Production.create_menu()

      menu
    end

    test "list_menus/0 returns all menus" do
      menu = menu_fixture()
      assert Production.list_menus() == [menu]
    end

    test "get_menu!/1 returns the menu with given id" do
      menu = menu_fixture()
      assert Production.get_menu!(menu.id) == menu
    end

    test "create_menu/1 with valid data creates a menu" do
      assert {:ok, %Menu{} = menu} = Production.create_menu(@valid_attrs)
      assert menu.category == "some category"
      assert menu.contains_egg == true
      assert menu.contains_meat == true
      assert menu.description == "some description"
      assert menu.ingredients == []
      assert menu.is_available == true
      assert menu.name == "some name"
      assert menu.price == 42
    end

    test "create_menu/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Production.create_menu(@invalid_attrs)
    end

    test "update_menu/2 with valid data updates the menu" do
      menu = menu_fixture()
      assert {:ok, menu} = Production.update_menu(menu, @update_attrs)
      assert %Menu{} = menu
      assert menu.category == "some updated category"
      assert menu.contains_egg == false
      assert menu.contains_meat == false
      assert menu.description == "some updated description"
      assert menu.ingredients == []
      assert menu.is_available == false
      assert menu.name == "some updated name"
      assert menu.price == 43
    end

    test "update_menu/2 with invalid data returns error changeset" do
      menu = menu_fixture()
      assert {:error, %Ecto.Changeset{}} = Production.update_menu(menu, @invalid_attrs)
      assert menu == Production.get_menu!(menu.id)
    end

    test "delete_menu/1 deletes the menu" do
      menu = menu_fixture()
      assert {:ok, %Menu{}} = Production.delete_menu(menu)
      assert_raise Ecto.NoResultsError, fn -> Production.get_menu!(menu.id) end
    end

    test "change_menu/1 returns a menu changeset" do
      menu = menu_fixture()
      assert %Ecto.Changeset{} = Production.change_menu(menu)
    end
  end
end
