defmodule DIWeb.AddressControllerTest do
  use DIWeb.ConnCase

  alias DI.Consumption
  alias DI.Consumption.Address

  @create_attrs %{city: "some city", label: "some label", landmark: "some landmark", latitude: 120.5, longitude: 120.5, pincode: "some pincode", street: "some street"}
  @update_attrs %{city: "some updated city", label: "some updated label", landmark: "some updated landmark", latitude: 456.7, longitude: 456.7, pincode: "some updated pincode", street: "some updated street"}
  @invalid_attrs %{city: nil, label: nil, landmark: nil, latitude: nil, longitude: nil, pincode: nil, street: nil}

  def fixture(:address) do
    {:ok, address} = Consumption.create_address(@create_attrs)
    address
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all addresses", %{conn: conn} do
      conn = get conn, address_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create address" do
    test "renders address when data is valid", %{conn: conn} do
      conn = post conn, address_path(conn, :create), address: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, address_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "city" => "some city",
        "label" => "some label",
        "landmark" => "some landmark",
        "latitude" => 120.5,
        "longitude" => 120.5,
        "pincode" => "some pincode",
        "street" => "some street"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, address_path(conn, :create), address: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update address" do
    setup [:create_address]

    test "renders address when data is valid", %{conn: conn, address: %Address{id: id} = address} do
      conn = put conn, address_path(conn, :update, address), address: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, address_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "city" => "some updated city",
        "label" => "some updated label",
        "landmark" => "some updated landmark",
        "latitude" => 456.7,
        "longitude" => 456.7,
        "pincode" => "some updated pincode",
        "street" => "some updated street"}
    end

    test "renders errors when data is invalid", %{conn: conn, address: address} do
      conn = put conn, address_path(conn, :update, address), address: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete address" do
    setup [:create_address]

    test "deletes chosen address", %{conn: conn, address: address} do
      conn = delete conn, address_path(conn, :delete, address)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, address_path(conn, :show, address)
      end
    end
  end

  defp create_address(_) do
    address = fixture(:address)
    {:ok, address: address}
  end
end
