defmodule DIWeb.CourierControllerTest do
  use DIWeb.ConnCase

  alias DI.Logistics
  alias DI.Logistics.Courier

  @create_attrs %{is_active: true, name: "some name", phone: "some phone"}
  @update_attrs %{is_active: false, name: "some updated name", phone: "some updated phone"}
  @invalid_attrs %{is_active: nil, name: nil, phone: nil}

  def fixture(:courier) do
    {:ok, courier} = Logistics.create_courier(@create_attrs)
    courier
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all couriers", %{conn: conn} do
      conn = get conn, courier_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create courier" do
    test "renders courier when data is valid", %{conn: conn} do
      conn = post conn, courier_path(conn, :create), courier: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, courier_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "is_active" => true,
        "name" => "some name",
        "phone" => "some phone"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, courier_path(conn, :create), courier: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update courier" do
    setup [:create_courier]

    test "renders courier when data is valid", %{conn: conn, courier: %Courier{id: id} = courier} do
      conn = put conn, courier_path(conn, :update, courier), courier: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, courier_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "is_active" => false,
        "name" => "some updated name",
        "phone" => "some updated phone"}
    end

    test "renders errors when data is invalid", %{conn: conn, courier: courier} do
      conn = put conn, courier_path(conn, :update, courier), courier: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete courier" do
    setup [:create_courier]

    test "deletes chosen courier", %{conn: conn, courier: courier} do
      conn = delete conn, courier_path(conn, :delete, courier)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, courier_path(conn, :show, courier)
      end
    end
  end

  defp create_courier(_) do
    courier = fixture(:courier)
    {:ok, courier: courier}
  end
end
