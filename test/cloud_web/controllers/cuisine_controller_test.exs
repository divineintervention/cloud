defmodule DIWeb.CuisineControllerTest do
  use DIWeb.ConnCase

  alias DI.Production
  alias DI.Production.Cuisine

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:cuisine) do
    {:ok, cuisine} = Production.create_cuisine(@create_attrs)
    cuisine
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all cuisines", %{conn: conn} do
      conn = get conn, cuisine_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create cuisine" do
    test "renders cuisine when data is valid", %{conn: conn} do
      conn = post conn, cuisine_path(conn, :create), cuisine: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, cuisine_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "name" => "some name"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, cuisine_path(conn, :create), cuisine: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update cuisine" do
    setup [:create_cuisine]

    test "renders cuisine when data is valid", %{conn: conn, cuisine: %Cuisine{id: id} = cuisine} do
      conn = put conn, cuisine_path(conn, :update, cuisine), cuisine: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, cuisine_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "name" => "some updated name"}
    end

    test "renders errors when data is invalid", %{conn: conn, cuisine: cuisine} do
      conn = put conn, cuisine_path(conn, :update, cuisine), cuisine: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete cuisine" do
    setup [:create_cuisine]

    test "deletes chosen cuisine", %{conn: conn, cuisine: cuisine} do
      conn = delete conn, cuisine_path(conn, :delete, cuisine)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, cuisine_path(conn, :show, cuisine)
      end
    end
  end

  defp create_cuisine(_) do
    cuisine = fixture(:cuisine)
    {:ok, cuisine: cuisine}
  end
end
