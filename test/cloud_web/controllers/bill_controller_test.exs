defmodule DIWeb.BillControllerTest do
  use DIWeb.ConnCase

  alias DI.Logistics
  alias DI.Logistics.Bill

  @create_attrs %{is_collected: true}
  @update_attrs %{is_collected: false}
  @invalid_attrs %{is_collected: nil}

  def fixture(:bill) do
    {:ok, bill} = Logistics.create_bill(@create_attrs)
    bill
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all bills", %{conn: conn} do
      conn = get conn, bill_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create bill" do
    test "renders bill when data is valid", %{conn: conn} do
      conn = post conn, bill_path(conn, :create), bill: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, bill_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "is_collected" => true}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, bill_path(conn, :create), bill: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update bill" do
    setup [:create_bill]

    test "renders bill when data is valid", %{conn: conn, bill: %Bill{id: id} = bill} do
      conn = put conn, bill_path(conn, :update, bill), bill: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, bill_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "is_collected" => false}
    end

    test "renders errors when data is invalid", %{conn: conn, bill: bill} do
      conn = put conn, bill_path(conn, :update, bill), bill: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete bill" do
    setup [:create_bill]

    test "deletes chosen bill", %{conn: conn, bill: bill} do
      conn = delete conn, bill_path(conn, :delete, bill)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, bill_path(conn, :show, bill)
      end
    end
  end

  defp create_bill(_) do
    bill = fixture(:bill)
    {:ok, bill: bill}
  end
end
