defmodule DIWeb.VendorControllerTest do
  use DIWeb.ConnCase

  alias DI.Production
  alias DI.Production.Vendor

  @create_attrs %{address: "some address", closes_at: ~T[14:00:00.000000], is_open: true, is_verified: true, latitude: 120.5, locality: "some locality", longitude: 120.5, name: "some name", opens_at: ~T[14:00:00.000000], otp: "some otp", phone: "some phone", token: "some token"}
  @update_attrs %{address: "some updated address", closes_at: ~T[15:01:01.000000], is_open: false, is_verified: false, latitude: 456.7, locality: "some updated locality", longitude: 456.7, name: "some updated name", opens_at: ~T[15:01:01.000000], otp: "some updated otp", phone: "some updated phone", token: "some updated token"}
  @invalid_attrs %{address: nil, closes_at: nil, is_open: nil, is_verified: nil, latitude: nil, locality: nil, longitude: nil, name: nil, opens_at: nil, otp: nil, phone: nil, token: nil}

  def fixture(:vendor) do
    {:ok, vendor} = Production.create_vendor(@create_attrs)
    vendor
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all vendors", %{conn: conn} do
      conn = get conn, vendor_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create vendor" do
    test "renders vendor when data is valid", %{conn: conn} do
      conn = post conn, vendor_path(conn, :create), vendor: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, vendor_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "address" => "some address",
        "closes_at" => ~T[14:00:00.000000],
        "is_open" => true,
        "is_verified" => true,
        "latitude" => 120.5,
        "locality" => "some locality",
        "longitude" => 120.5,
        "name" => "some name",
        "opens_at" => ~T[14:00:00.000000],
        "otp" => "some otp",
        "phone" => "some phone",
        "token" => "some token"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, vendor_path(conn, :create), vendor: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update vendor" do
    setup [:create_vendor]

    test "renders vendor when data is valid", %{conn: conn, vendor: %Vendor{id: id} = vendor} do
      conn = put conn, vendor_path(conn, :update, vendor), vendor: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, vendor_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "address" => "some updated address",
        "closes_at" => ~T[15:01:01.000000],
        "is_open" => false,
        "is_verified" => false,
        "latitude" => 456.7,
        "locality" => "some updated locality",
        "longitude" => 456.7,
        "name" => "some updated name",
        "opens_at" => ~T[15:01:01.000000],
        "otp" => "some updated otp",
        "phone" => "some updated phone",
        "token" => "some updated token"}
    end

    test "renders errors when data is invalid", %{conn: conn, vendor: vendor} do
      conn = put conn, vendor_path(conn, :update, vendor), vendor: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete vendor" do
    setup [:create_vendor]

    test "deletes chosen vendor", %{conn: conn, vendor: vendor} do
      conn = delete conn, vendor_path(conn, :delete, vendor)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, vendor_path(conn, :show, vendor)
      end
    end
  end

  defp create_vendor(_) do
    vendor = fixture(:vendor)
    {:ok, vendor: vendor}
  end
end
