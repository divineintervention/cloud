defmodule DIWeb.BillItemControllerTest do
  use DIWeb.ConnCase

  alias DI.Logistics
  alias DI.Logistics.BillItem

  @create_attrs %{quantity: 42}
  @update_attrs %{quantity: 43}
  @invalid_attrs %{quantity: nil}

  def fixture(:bill_item) do
    {:ok, bill_item} = Logistics.create_bill_item(@create_attrs)
    bill_item
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all billitems", %{conn: conn} do
      conn = get conn, bill_item_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create bill_item" do
    test "renders bill_item when data is valid", %{conn: conn} do
      conn = post conn, bill_item_path(conn, :create), bill_item: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, bill_item_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "quantity" => 42}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, bill_item_path(conn, :create), bill_item: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update bill_item" do
    setup [:create_bill_item]

    test "renders bill_item when data is valid", %{conn: conn, bill_item: %BillItem{id: id} = bill_item} do
      conn = put conn, bill_item_path(conn, :update, bill_item), bill_item: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, bill_item_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "quantity" => 43}
    end

    test "renders errors when data is invalid", %{conn: conn, bill_item: bill_item} do
      conn = put conn, bill_item_path(conn, :update, bill_item), bill_item: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete bill_item" do
    setup [:create_bill_item]

    test "deletes chosen bill_item", %{conn: conn, bill_item: bill_item} do
      conn = delete conn, bill_item_path(conn, :delete, bill_item)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, bill_item_path(conn, :show, bill_item)
      end
    end
  end

  defp create_bill_item(_) do
    bill_item = fixture(:bill_item)
    {:ok, bill_item: bill_item}
  end
end
