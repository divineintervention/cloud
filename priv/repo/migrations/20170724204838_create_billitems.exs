defmodule DI.Repo.Migrations.CreateBillitems do
  use Ecto.Migration

  def change do
    create table(:billitems) do
      add :quantity, :integer
      add :bill_id, references(:bills, on_delete: :nothing)
      add :menu_id, references(:menus, on_delete: :nothing)

      timestamps()
    end

    create index(:billitems, [:bill_id])
    create index(:billitems, [:menu_id])
  end
end
