defmodule DI.Repo.Migrations.CreateVendors do
  use Ecto.Migration

  def change do
    create table(:vendors) do
      add :is_open, :boolean, default: false, null: false
      add :name, :string
      add :locality, :string
      add :address, :string
      add :opens_at, :time
      add :closes_at, :time
      add :token, :string
      add :phone, :string
      add :otp, :string
      add :is_verified, :boolean, default: false, null: false
      add :latitude, :float
      add :longitude, :float

      timestamps()
    end

  end
end
