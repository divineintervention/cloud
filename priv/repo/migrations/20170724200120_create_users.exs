defmodule DI.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :token, :string
      add :name, :string
      add :phone, :string
      add :otp, :string
      add :is_verified, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:users, [:phone])
  end
end
