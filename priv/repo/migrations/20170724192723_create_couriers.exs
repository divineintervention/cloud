defmodule DI.Repo.Migrations.CreateCouriers do
  use Ecto.Migration

  def change do
    create table(:couriers) do
      add :name, :string
      add :phone, :string
      add :is_active, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:couriers, [:phone])
  end
end
