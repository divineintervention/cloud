defmodule DI.Repo.Migrations.CreateAddresses do
  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :label, :string
      add :street, :string
      add :landmark, :string
      add :city, :string
      add :pincode, :string
      add :latitude, :float
      add :longitude, :float
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:addresses, [:user_id])
  end
end
