defmodule DI.Repo.Migrations.CreateDeliveries do
  use Ecto.Migration

  def change do
    create table(:deliveries) do
      add :is_complete, :boolean, default: false, null: false
      add :courier_id, references(:couriers, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)
      add :address_id, references(:addresses, on_delete: :nothing)

      timestamps()
    end

    create index(:deliveries, [:courier_id])
    create index(:deliveries, [:user_id])
    create index(:deliveries, [:address_id])
  end
end
