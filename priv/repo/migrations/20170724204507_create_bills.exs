defmodule DI.Repo.Migrations.CreateBills do
  use Ecto.Migration

  def change do
    create table(:bills) do
      add :is_collected, :boolean, default: false, null: false
      add :vendor_id, references(:vendors, on_delete: :nothing)
      add :delivery_id, references(:deliveries, on_delete: :nothing)

      timestamps()
    end

    create index(:bills, [:vendor_id])
    create index(:bills, [:delivery_id])
  end
end
