defmodule DI.Repo.Migrations.CreateMenus do
  use Ecto.Migration

  def change do
    create table(:menus) do
      add :name, :string
      add :price, :integer
      add :is_available, :boolean, default: false, null: false
      add :category, :string
      add :description, :string
      add :ingredients, {:array, :string}
      add :contains_egg, :boolean, default: false, null: false
      add :contains_meat, :boolean, default: false, null: false
      add :vendor_id, references(:vendors, on_delete: :nothing)
      add :cuisine_id, references(:cuisines, on_delete: :nothing)

      timestamps()
    end

    create index(:menus, [:vendor_id])
    create index(:menus, [:cuisine_id])
  end
end
