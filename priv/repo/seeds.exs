# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     DI.Repo.insert!(%DI.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# Cuisine
indian = DI.Repo.insert!(%DI.Production.Cuisine{
      name: "Indian"
})
chinese = DI.Repo.insert!(%DI.Production.Cuisine{
      name: "Chinese"
})

# Courier
ram = DI.Repo.insert!(%DI.Logistics.Courier{
      name: "Ram",
      phone: "9999999999",
      is_active: false
})
shyam = DI.Repo.insert!(%DI.Logistics.Courier{
      name: "Shyam",
      phone: "8888888888",
      is_active: false
})

# Vendor
zuber = DI.Repo.insert!(%DI.Production.Vendor{
      address: "Kaktives Road",
      closes_at: ~T(18:00:00),
      is_open: true,
      is_verified: true,
      latitude: 15.864633,
      locality: "Kaktives Road",
      longitude: 74.511542,
      name: "Zuber Biryaniwala",
      opens_at: ~T(10:00:00),
      otp: "123456",
      phone: "7777777777",
      token: "token"
})
parathacorner = DI.Repo.insert!(%DI.Production.Vendor{
      address: "Khanapur Road",
      closes_at: ~T(18:00:00),
      is_open: true,
      is_verified: true,
      latitude: 15.829473,
      locality: "Khanapur Road",
      longitude: 74.499571,
      name: "Paratha Corner",
      opens_at: ~T(10:00:00),
      otp: "123456",
      phone: "6666666666",
      token: "token"
})

# Menu
biryani = DI.Repo.insert!(%DI.Production.Menu{
      category: "Rice",
      contains_egg: true,
      contains_meat: true,
      description: "Spicy chicken cooked in tender rice",
      ingredients: ~W(Chicken Rice Spices),
      is_available: true,
      name: "Chicken Biryani",
      price: 100,
      vendor_id: zuber.id,
      cuisine_id: indian.id
})
shawarma = DI.Repo.insert!(%DI.Production.Menu{
      category: "Rolls",
      contains_egg: true,
      contains_meat: true,
      description: "Shredded, roasted, tender chicken wrapped in a pita with a herbed-mayo spread",
      ingredients: ~W(Chicken Pita Mayo),
      is_available: true,
      name: "Chicken Shawarma",
      price: 100,
      vendor_id: zuber.id,
      cuisine_id: indian.id
})
paratha = DI.Repo.insert!(%DI.Production.Menu{
      category: "Breads",
      contains_egg: false,
      contains_meat: false,
      description: "The usual paratha with no surprises",
      ingredients: ~W(Wheatflour Ajwain Salt Spices),
      is_available: true,
      name: "Plain paratha",
      price: 50,
      vendor_id: parathacorner.id,
      cuisine_id: indian.id
})
curry = DI.Repo.insert!(%DI.Production.Menu{
      category: "Curry",
      contains_egg: false,
      contains_meat: false,
      description: "Paneer and vegetables cooked in a 'Kadhai' curry with spices",
      ingredients: ~W(Paneer Vegetables Salt Spices),
      is_available: true,
      name: "Kadhai paneer",
      price: 110,
      vendor_id: parathacorner.id,
      cuisine_id: indian.id
})
